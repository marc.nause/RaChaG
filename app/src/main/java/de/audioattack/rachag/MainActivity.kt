package de.audioattack.rachag

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

private val ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()

class MainActivity : AppCompatActivity() {

    private lateinit var mainHandler: Handler
    private lateinit var tvCharacter: TextView
    private var active = true

    private val updateTextTask = Runnable {
        updateCharacter()
        scheduleTask()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainHandler = Handler(Looper.getMainLooper())
        tvCharacter = findViewById(R.id.character)

        if (savedInstanceState != null) {
            active = savedInstanceState.getBoolean("active")
            tvCharacter.text =
                savedInstanceState.getCharSequence("character", "${ALPHABET.first()}")
        } else {
            tvCharacter.text = "${ALPHABET.first()}"
        }
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacks(updateTextTask)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("active", active)
        outState.putCharSequence("character", tvCharacter.text)
    }

    override fun onResume() {
        super.onResume()

        scheduleTask()

        tvCharacter.setOnTouchListener(View.OnTouchListener { _: View, _: MotionEvent ->
            run {
                active = !active
                scheduleTask()
            }
            return@OnTouchListener false
        })
    }

    private fun scheduleTask() {
        if (active) {
            mainHandler.postDelayed(updateTextTask, 100L)
        } else {
            mainHandler.removeCallbacks(updateTextTask)
        }
    }

    private fun updateCharacter() {
        tvCharacter.text = "${ALPHABET.random()}"
    }
}